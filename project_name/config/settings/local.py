from .common import *

# Database
# https://docs.djangoproject.com/en/{{ docs_version }}/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{ project_name }}_db',
        'USER': '{{ project_name }}_user',
        'PASSWORD': '{{ project_name }}_password',
        'HOST': '127.0.0.1',
        'PORT': '5432'
    }
}
